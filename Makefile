ASMFLAGS=-felf64

.PHONY: clean, test

lib.o: lib.asm
	nasm $(ASMFLAGS) -o $@ $<

dict.o: dict.asm
	nasm $(ASMFLAGS) -o $@ $<

main.o: main.asm
	nasm $(ASMFLAGS) -o $@ $<

program: main.o lib.o dict.o
	ld -o $@ $^

clean:
	rm -rf ./*.o program

test:
	python3 test.py
